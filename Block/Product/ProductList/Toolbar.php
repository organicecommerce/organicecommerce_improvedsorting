<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OrganicEcommerce\ImprovedSorting\Block\Product\ProductList;

class Toolbar extends \Magento\Catalog\Block\Product\ProductList\Toolbar
{
    /**
     * Set collection to pager
     *
     * @param \Magento\Framework\Data\Collection $collection
     * @return $this
     */
    public function setCollection($collection)
    {
        $this->_collection = $collection;

        $this->_collection->setCurPage($this->getCurrentPage());

        // we need to set pagination only if passed value integer and more that 0
        $limit = (int)$this->getLimit();
        if ($limit) {
            $this->_collection->setPageSize($limit);
        }
        if ($this->getCurrentOrder()) {
            /*if (($this->getCurrentOrder()) == 'position') {
                $this->_collection->addAttributeToSort(
                    $this->getCurrentOrder(),
                    $this->getCurrentDirection()
                );*/
            if (($this->getCurrentOrder()) == 'bestselling') {
                $this->_collection->getSelect()->joinLeft( 
                    'sales_order_item', 
                    'e.entity_id = sales_order_item.product_id', 
                    array('qty_ordered'=>'SUM(sales_order_item.qty_ordered)')) 
                    ->group('e.entity_id') 
                    ->order('qty_ordered desc');
            } else if (($this->getCurrentOrder()) == 'price_low_to_high') {
                $this->_collection->setOrder(
                    'price',
                    'asc'
                );
            } else if (($this->getCurrentOrder()) == 'price_high_to_low') {
                $this->_collection->setOrder(
                    'price',
                    'desc'
                );
            } else if (($this->getCurrentOrder()) == 'newly_listed') {
                $this->_collection->setOrder(
                    'created_at',
                    'desc'
                );
            } else if (($this->getCurrentOrder()) == 'vintage_ascending') {
                $this->_collection->setOrder(
                    'hg_wine_vintage',
                    'asc'
                );
            } else if (($this->getCurrentOrder()) == 'vintage_descending') {
                $this->_collection->setOrder(
                    'hg_wine_vintage',
                    'desc'
                );
            } else if (($this->getCurrentOrder()) == 'name_a_z') {
                $this->_collection->setOrder(
                    'name',
                    'asc'
                );
            } else if (($this->getCurrentOrder()) == 'name_z_a') {
                $this->_collection->setOrder(
                    'name',
                    'desc'
                );
            } else {
                $this->_collection->setOrder($this->getCurrentOrder(), $this->getCurrentDirection());
            }
        }
        return $this;
    }
}
