<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace OrganicEcommerce\ImprovedSorting\Model;

class Config extends \Magento\Catalog\Model\Config
{
    /**
     * Retrieve Attributes Used for Sort by as array
     * key = code, value = name
     *
     * @return array
     */
    public function getAttributeUsedForSortByArray()
    {
        //$options = ['position' => __('Position')];
        
        $options = ['bestselling' => __('Bestselling')];
        $options['price_low_to_high'] = __('Price Low To High');
        $options['price_high_to_low'] = __('Price High To Low');
        $options['newly_listed'] = __('Newly Listed');
        $options['vintage_ascending'] = __('Vintage Ascending');
        $options['vintage_descending'] = __('Vintage Descending');
        $options['name_a_z'] = __('Name A - Z');
        $options['name_z_a'] = __('Name Z - A');
        
        //foreach ($this->getAttributesUsedForSortBy() as $attribute) {
            /* @var $attribute \Magento\Eav\Model\Entity\Attribute\AbstractAttribute */
        //    $options[$attribute->getAttributeCode()] = $attribute->getStoreLabel();
        //}

        return $options;
    }
}
